<!--
SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

(configuration)=

# Configuration options

## Configuration settings

The following settings in your djangos `settings.py` have an effect on the app:

```{eval-rst}
.. automodulesumm:: djangocms_host_state.app_settings
    :autosummary-no-titles:
```
