<!--
SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

(installation)=
# Installation

```{warning}

This page has been automatically generated as has not yet been reviewed
by the authors of djangocms-host-state!
```


To install the `djangocms-host-state` package for your Django
project, you need to follow two steps:

1. {ref}`Install the package <install-package>`
2. {ref}`Add the app to your Django project <install-django-app>`


(install-package)=

## Installation from PyPi

The recommended way to install this package is via pip and PyPi via:

```bash
pip install djangocms-host-state
```

or, if you are using `pipenv`, via:

```bash
pipenv install djangocms-host-state
```

Or install it directly from
[the source code repository on Gitlab][source code repository] via:

```bash
pip install git+https://codebase.helmholtz.cloud/hcdc/django/djangocms-host-state.git
```

The latter should however only be done if you want to access the
development versions  (see {ref}`install-develop`).

(install-django-app)=

## Install the Django App for your project

To use the `djangocms-host-state` package in your Django project,
you need to add the app to your `INSTALLED_APPS`, configure your `urls.py`, run
the migration, add a login button in your templates. Here are the step-by-step
instructions:

1. Add the `djangocms_host_state` app to your `INSTALLED_APPS`
2. in your projects urlconf (see {setting}`ROOT_URLCONF`), add include
   {mod}`djangocms_host_state.urls` via::

   ```python
    from django.urls import include, path

    urlpatterns += [
        path("djangocms-host-state/", include("djangocms_host_state.urls")),
    ]
    ```
3. Run ``python manage.py migrate`` to add models to your database
4. Configure the app to your needs (see {ref}`configuration`) (you can also
   have a look into the `testproject` folder in the
   [source code repository][source code repository] for a possible
   configuration).

If you need a deployment-ready django setup for an app like this, please
have a look at the following template:
https://codebase.helmholtz.cloud/hcdc/software-templates/django-docker-template

That's it!

[source code repository]: https://codebase.helmholtz.cloud/hcdc/django/djangocms-host-state

(install-develop)=
## Installation for development

Please head over to our
`contributing guide <contributing>`{.interpreted-text role="ref"} for
installation instruction for development.
