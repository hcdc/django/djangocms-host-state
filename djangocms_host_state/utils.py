# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: LGPL-3.0-or-later

"""Utility functions"""
from __future__ import annotations

import socket
from typing import TYPE_CHECKING, Tuple

if TYPE_CHECKING:
    from djangocms_host_state.models import Host


def restart_required() -> bool:
    """Check if the host requires a restart"""
    from djangocms_host_state.models import Host

    hostname = socket.gethostname()
    return Host.objects.filter(
        hostname=hostname, requires_restart=True
    ).exists()


def mark_hosts_for_restart():
    from djangocms_host_state.models import Host

    hosts = list(Host.objects.filter(requires_restart=False))
    for host in hosts:
        host.requires_restart = True
    Host.objects.bulk_update(hosts, ["requires_restart"])


def get_or_create_host() -> Tuple[Host, bool]:
    from djangocms_host_state.models import Host

    hostname = socket.gethostname()
    return Host.objects.get_or_create(hostname=hostname)
