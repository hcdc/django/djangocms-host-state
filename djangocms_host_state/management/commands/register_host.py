# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: LGPL-3.0-or-later

from __future__ import annotations

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    """Django command to register the host."""

    help = "Register the host in the database/"

    def handle(self, *args, **kwargs):
        from djangocms_host_state import utils

        host, created = utils.get_or_create_host()
        if not created:
            host.restart_required = False
            host.save()
