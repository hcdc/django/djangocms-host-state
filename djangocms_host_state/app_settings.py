# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: LGPL-3.0-or-later

"""App settings
------------

This module defines the settings options for the
``djangocms-host-state`` app.
"""


from __future__ import annotations

from django.conf import settings  # noqa: F401
