# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: LGPL-3.0-or-later

from typing import Dict

from djangocms_host_state import utils


def check_host_state(request=None) -> Dict:
    """Check if the host requires a restart"""
    return {"restart_required": utils.restart_required()}
