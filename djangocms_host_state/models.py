# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: LGPL-3.0-or-later

"""Models
------

Models for the djangocms-host-state app.
"""

from __future__ import annotations

from cms.signals import urls_need_reloading
from django.db import models  # noqa: F401
from django.dispatch import receiver

from djangocms_host_state import utils


class Host(models.Model):
    """A host that deploys the application"""

    hostname = models.CharField(
        max_length=500, help_text="The name of the host", unique=True
    )

    requires_restart = models.BooleanField(
        default=False, help_text="Does the host require a restart?"
    )

    def __str__(self) -> str:
        return self.hostname


@receiver(urls_need_reloading)
def mark_host_for_restart(**kwargs):
    utils.mark_hosts_for_restart()
