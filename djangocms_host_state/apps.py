# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: LGPL-3.0-or-later

"""App config
----------

App config for the djangocms_host_state app.
"""

from django.apps import AppConfig


class DjangoCMSHostStateConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "djangocms_host_state"
