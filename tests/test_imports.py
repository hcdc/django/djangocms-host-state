# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: LGPL-3.0-or-later

"""Test file for imports."""


def test_package_import():
    """Test the import of the main package."""
    import djangocms_host_state  # noqa: F401
